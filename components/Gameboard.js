import React, {useState, useEffect, useRef} from 'react';
import {Text, View, Pressable, Animated, ScrollView} from 'react-native';
import Entypo from '@expo/vector-icons/Entypo';
import styles from '../style/style.js';

export default function Gameboard() {
    const NUMBER_OF_ROWS = 5;
    const NUMBER_OF_COLS = 5;
    const START = 'air';
    const MISS = 'circle-with-cross';
    const HIT = 'hair-cross';
    const statusMessages = [
        'Game has not started.',
        'Press the start button first...',
        'Game is on...',
        'You sank all ships!',
        'Game over. Ships remaining.',
        'Timeout. Ships remaining.'
    ]
    var boardInitial = [];
    for (let i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLS; i++) {
        boardInitial[i] = START;
    }

    const [ships, setShips] = useState(putRandomShips());
    const [board, setBoard] = useState(boardInitial);
    const [status, setStatus] = useState(statusMessages[0]);
    const [shipAmount, setShipAmount] = useState(3);
    const [hitAmount, setHitAmount] = useState(0);
    const [bombAmount, setBombAmount] = useState(15);
    const [time, setTime] = useState(0);
    const timerRef = useRef();
    const scaleAnimation = [];
    for (let i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLS; i++) {
        scaleAnimation[i] = useRef(new Animated.Value(1)).current;
    }

    // for creating gameboard dynamically
    const fieldItems = [];
    for (let x = 0; x < NUMBER_OF_ROWS; x++) {
        var fieldCol = []
        for (let y = 0; y < NUMBER_OF_COLS; y++) {
            fieldCol.push(
                // animation included
                <Animated.View key={x * NUMBER_OF_COLS + y} style={
                    {
                        transform: [
                            {
                                scale: scaleAnimation[x * NUMBER_OF_COLS + y].interpolate({
                                inputRange: [1, 2, 3],
                                outputRange: [1,1.4,1]
                                })
                            }
                        ]
                    }
                }>
                    <Pressable key={x * NUMBER_OF_COLS + y} style={styles.row} onPress={() => drawItem(x * NUMBER_OF_COLS + y)}>
                        <Entypo key={x * NUMBER_OF_COLS + y} name={board[x * NUMBER_OF_COLS + y]} size={32} color={chooseItemColor(x * NUMBER_OF_COLS + y)} />
                    </Pressable>
                </Animated.View>
            );
        }

        var fieldRow =
            <View style={styles.flex} key={"row" + x}>
                {fieldCol.map((item) => item)}
            </View>;

        fieldItems.push(fieldRow);
    }

    function scale(index) {
        Animated.spring(scaleAnimation[index], {
          toValue: 3,
          duration: 2500,
          useNativeDriver: true
        }).start();
    }

    function drawItem(number) {
        if (status == statusMessages[0] || status == statusMessages[1]) {
            setStatus(statusMessages[1]);
            return;
        }
        if (board[number] === START && bombAmount > 0 && hitAmount < 3 && time < 30) {
            scale(number);
            setBombAmount(prev => prev - 1);
            if (ships[number] == 1) {
                board[number] = HIT;
                setShipAmount(prev => prev - 1);
                setHitAmount(prev => prev + 1);
            }
            else {
                board[number] = MISS;
            }
        }
    }

    useEffect(() => {
        checkGameEnd();
    }, [hitAmount, bombAmount, time, status])

    function checkGameEnd() {
        if (hitAmount == 3) {
            setStatus(statusMessages[3]);
            clearInterval(timerRef.current);
        }
        else if (bombAmount == 0) {
            setStatus(statusMessages[4]);
            clearInterval(timerRef.current);
        }
        else if (time == 30) {
            setStatus(statusMessages[5]);
            clearInterval(timerRef.current);
        }
    }

    function chooseItemColor(number) {
        if (board[number] === MISS) {
            return '#FF3031';
        }
        else if (board[number] === HIT) {
            return '#45CE30';
        }
        else {
            return "#74B9FF";
        }
    }

    function resetGame() {
        if (status != statusMessages[0] && status != statusMessages[1]) {
            setShips(putRandomShips());
            setBoard(boardInitial);
            setStatus(statusMessages[0]);
            setShipAmount(3);
            setHitAmount(0);
            setBombAmount(15);
            setTime(0);
            clearInterval(timerRef.current);

            for (let i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLS; i++) {
                scaleAnimation[i].setValue(1);
            }
        }
        else {
            setStatus(statusMessages[2]);
            const timer = setInterval(() => {
                setTime(prev => prev + 1);
            }, 1000);
            timerRef.current = timer;
        }
    }

    function putRandomShips() {
        var boardShips = [];
        for (let i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLS; i++) {
            boardShips[i] = 0;
        }

        var shipCount = 0;
        while(shipCount < 3) {
            let random = Math.floor(Math.random() * boardInitial.length);
            if (boardShips[random] == 0) {
                boardShips[random] = 1;
                shipCount++;
            }
        }

        return boardShips;
    }

    return(
        <ScrollView>
            <View style={styles.gameboard}>
                {fieldItems.map((fieldItem) => fieldItem)}
                <Pressable style={styles.button} onPress={() => resetGame()}>
                    <Text style={styles.buttonText}>{status == statusMessages[0] || status == statusMessages[1] ? 'Start game' : 'New game'}</Text>
                </Pressable>
                <View style={styles.text}>
                    <Text style={{color: 'white'}}>Hits: {hitAmount}     Bombs: {bombAmount}     Ships: {shipAmount}</Text>
                </View>
                <View style={styles.text}>
                    <Text style={{color: 'white'}}>Time: {time} sec</Text>
                </View>
                <View style={styles.text}>
                    <Text style={{color: 'white'}}>Status: {status}</Text>
                </View>
            </View>
        </ScrollView>
    )
}
