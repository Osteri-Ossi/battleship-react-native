import React from 'react';
import { View, SafeAreaView, StatusBar, Platform } from 'react-native';
import Header from './components/Header.js';
import Footer from './components/Footer.js';
import Gameboard from './components/Gameboard.js';
import styles from './style/style.js';
import Constants from 'expo-constants';

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
       <StatusBar style={{paddingTop: Platform.OS === 'android' ? Constants.statusBarHeight : 0}} backgroundColor="black" />
      <Header />
      <Gameboard />
      <Footer />
    </SafeAreaView>
  );
}